import { Component } from '@angular/core';
import { CrDialogService } from 'projects/cr-dialog/src/public-api';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'cr-dialog-sandbox';
  constructor(private dialog: CrDialogService) {

  }

  openAlert() {
    // const instance = this.dialog.alert('Alert is CLicked', { data: { color: 'accent' } });
    const instance = this.dialog.alert('Alert is CLicked',
      {
        data:
        {
          // color: 'accent'
          type: 'info'
        }
      }
    );
    instance
      .afterClosed()
      .subscribe(result => {
        console.log('The dialog was closed');
      });
  }

  openPrompt() {
    const instance = this.dialog.prompt('Some Message');
    instance
      .afterClosed()
      .subscribe(result => {
        console.log('The dialog was closed');
      });
  }

  openConfirm() {
    const instance = this.dialog.confirm('Are you sure you want to delete?');
    instance
      .afterClosed()
      .subscribe(result => {
        if (result) {
          this.dialog.alert('Ok Deleted');
        }
        console.log('The dialog was closed', result);
      });
  }
}
