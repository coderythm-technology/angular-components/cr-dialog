import { TestBed } from '@angular/core/testing';

import { CrDialogService } from './cr-dialog.service';

describe('CrDialogService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CrDialogService = TestBed.get(CrDialogService);
    expect(service).toBeTruthy();
  });
});
