import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrDialogComponent } from './cr-dialog.component';

describe('CrDialogComponent', () => {
  let component: CrDialogComponent;
  let fixture: ComponentFixture<CrDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
