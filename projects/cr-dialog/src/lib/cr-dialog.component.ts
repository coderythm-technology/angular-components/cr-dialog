import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'cr-dialog',
  templateUrl: './cr-dialog.component.html',
  styleUrls: ['./cr-dialog.component.scss']
})
export class CrDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<CrDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
  }

  clickYes() {
    this.dialogRef.close(true);
  }

  close(): void {
    this.dialogRef.close();
  }


}
