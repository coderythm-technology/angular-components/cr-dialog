import { Injectable } from '@angular/core';
import { CrDialogComponent } from './cr-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import * as _ from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class CrDialogService {

  animal: string;
  name: string;

  dialogInstance;

  constructor(public dialog: MatDialog) { }

  openDialog(data, params): void {
    const defaultParams = {
      width: '300px',
      data
    };
    this.dialogInstance = this.dialog.open(
      CrDialogComponent,
      _.merge(defaultParams, params)
    );
  }

  alert(message = '', params = {}) {
    this.openDialog({
      dialogType: 'alert',
      message
    }, params);
    return this.dialogInstance;
  }

  prompt(message = '', params = {}) {
    this.openDialog({
      dialogType: 'prompt',
      message
    }, params);
    return this.dialogInstance;
  }

  confirm(message = '', params = {}) {
    this.openDialog({
      dialogType: 'confirm',
      message
    }, params);
    return this.dialogInstance;
  }

  close() {
    this.dialogInstance.close();
  }
}
